.macro delay100ms
; Delay 800 000 cycles
; 100ms at 8.0 MHz

    PUSH R18
	PUSH R19
	PUSH R20
	ldi  r18, 5
    ldi  r19, 15
    ldi  r20, 242
l1: dec  r20
    brne l1
    dec  r19
    brne l1
    dec  r18
    brne l1
	POP R20
	POP R19
	POP R18
.endm

.macro delay500ms
; Delay 4 000 000 cycles
; 500ms at 8.0 MHz

    PUSH R18
	PUSH R19
	PUSH R20
	ldi  r18, 21
    ldi  r19, 75
    ldi  r20, 191
l1: dec  r20
    brne l1
    dec  r19
    brne l1
    dec  r18
    brne l1
    nop
	POP R20
	POP R19
	POP R18
.endm

.macro delay1s
; Delay 8 000 000 cycles
; 1s at 8.0 MHz

    PUSH R18
	PUSH R19
	PUSH R20
	ldi  r18, 41
    ldi  r19, 150
    ldi  r20, 128
l1: dec  r20
    brne l1
    dec  r19
    brne l1
    dec  r18
    brne l1
	POP R20
	POP R19
	POP R18
.endm