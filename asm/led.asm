.include "delay.inc"

.equ PORTA_SRAM = 0x3b
.equ PORTB_SRAM = 0x38
.equ PORTC_SRAM = 0x35
.equ PORTD_SRAM = 0x32

.dseg
;------- data segment -----------

.cseg
;------- code segment -----------
;---- interrupt vector table ----
.org 0x0000 ; Reset Vector
RJMP start
.org 0x0002 ; External Interrupt Request 0
RETI
.org 0x0004 ; External Interrupt Request 1
RETI
.org 0x0006 ; Timer/Counter2 Compare Match
RETI
.org 0x0008 ; Timer/Counter2 Overflow
RETI
.org 0x000a ; Timer/Counter1 Capture Event
RETI
.org 0x000c ; Timer/Counter1 Compare Match A
RETI
.org 0x000e ; Timer/Counter1 Compare Match B
RETI
.org 0x0010 ; Timer/Counter1 Overflow
RETI
.org 0x0012 ; Timer/Counter0 Overflow
RETI
.org 0x0014 ; Serial Transfer Complete
RETI
.org 0x0016 ; USART, Rx Complete
RETI
.org 0x0018 ; USART Data Register Empty
RETI
.org 0x001a ; USART, Tx Complete
RETI
.org 0x001c ; ADC Conversion Complete
RETI
.org 0x001e ; EEPROM Ready
RETI
.org 0x0020 ; Analog Comparator
RETI
.org 0x0022 ; 2-wire Serial Interface
RETI
.org 0x0024 ; External Interrupt Request 2
RETI
.org 0x0026 ; Timer/Counter0 Compare Match
RETI
.org 0x0028 ; Store Program Memory Ready
RETI
;---------------------------

start:
	LDI R16, Low(RAMEND) ; stack init
	OUT SPL, R16
	LDI R16, High(RAMEND)
	OUT SPH, R16

	LDI R16, 0xff ; all ports out
	OUT DDRA, R16
	OUT DDRB, R16
	OUT DDRC, R16
	OUT DDRD, R16

led_start:

outer:
	LDI R18, 0 ; column (outer)
inner:
	LDI R17, 0 ; row (inner)
loop:
	LDI R19, PORTB_SRAM
	LDI R20, PORTA_SRAM
	PUSH R19
	PUSH R20
	PUSH R18
	PUSH R17
	RCALL led_on
	POP R17
	POP R18
	POP R20
	POP R19
	delay100ms

	INC R17
	CPI R17, 8
	BRNE loop
	INC R18
	CPI R18, 8
	BRNE inner

	RJMP led_start

;-------- procedures ---------------

;----------------------------------
; Procedure: Turn led on
; Arguments (pop order):
; - row
; - colum
; - power port address
; - ground port address
; Returns: void
;----------------------------------
led_on:
	PUSH ZL
	PUSH ZH

	IN ZL, SPL
	IN ZH, SPH
	INC ZL
	INC ZL
	INC ZL
	INC ZL
	INC ZL

	PUSH R18
	LD R18, Z+ ; row
	PUSH R19
	LD R19, Z+ ; column
	PUSH R26
	LD R26, Z+ ; power port address
	PUSH R28
	LD R28, Z+ ; ground port address

	PUSH R16
	LDI R16, 0
	ST X, R16
	LDI R16, (1 << 0)
loop_row:
	CPI R18, 0
	BREQ exit_loop_row
	LSL R16
	DEC R18
	RJMP loop_row
exit_loop_row:
	ST X, R16
	LDI R16, 0xff
	ST Y, R16
	LDI R16, (1 << 0)
loop_column:
	CPI R19, 0
	BREQ exit_loop_column
	LSL R16
	DEC R19
	RJMP loop_column
exit_loop_column:
	PUSH R17
	LDI R17, 0xff
	EOR R16, R17
	ST Y, R16

	POP R17
	POP R16
	POP R28
	POP R26
	POP R19
	POP R18
	POP ZH
	POP ZL

	RET
;---------- EEPROM segment -----------
.ESEG