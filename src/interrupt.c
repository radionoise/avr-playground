#include "interrupt.h"
#include "led.h"
#include <avr/interrupt.h>

#define TIMEOUT 100

#define TRUE 1
#define FALSE 0

typedef int8_t BOOLEAN;

BOOLEAN forward = TRUE;
volatile uint8_t * port = &PORTA;

ISR(INT0_vect) {
    forward = ~forward;
}

ISR(INT1_vect) {
    if (port == &PORTA) {
        port = &PORTB;
    } else if (port == &PORTB) {
        port = &PORTC;
    } else {
        port = &PORTA;
    }
}

void ledRunWithModeSwitch() {
    uint8_t allOut = 0b11111111;
    DDRA = allOut;
    DDRB = allOut;
    DDRC = allOut;
    DDRD = allOut;

    DDRD &= ~(1 << PD2); // enable PD2 input. DDRD = 0b11111011
    PORTD |= (1 << PD2); // enable internal pull-up resistors on PD2
    MCUCR |= (0 << ISC00) | (1 << ISC01); // descending level INT0

    DDRD &= ~(1 << PD3); // enable PD2 input. DDRD = 0b11110011
    PORTD |= (1 << PD3); // enable internal pull-up resistors on PD3
    MCUCR |= (0 << ISC10) | (1 << ISC11); // descending level INT1

    GICR |= (1 << INT0); // enable INT0
    GICR |= (1 << INT1); // enable INT1
    sei(); // enable all

    while (TRUE) {
        if (forward == TRUE) {
            ledRunForward(port);
        } else {
            ledRunBackward(port);
        }
    }
}