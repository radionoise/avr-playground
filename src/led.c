#include "led.h"
#include <util/delay.h>

#define TIMEOUT 100

void ledRunForwardBackward() {
    uint8_t allOut = 0b11111111;
    DDRA = allOut;
    DDRB = allOut;
    DDRC = allOut;
    DDRD = allOut;

    while (1) {
        ledRunForward(&PORTA);
        ledRunBackward(&PORTB);
        ledRunForward(&PORTC);
        ledRunBackward(&PORTD);

        ledRunForward(&PORTD);
        ledRunBackward(&PORTC);
        ledRunForward(&PORTB);
        ledRunBackward(&PORTA);
    }
}

void ledRunForward(volatile uint8_t *port) {
    for (int8_t pos = 0; pos <= 7; pos++) {
        ledOperate(port, pos);
    }

    *port = 0;
}

void ledRunBackward(volatile uint8_t *port) {
    for (int8_t pos = 7; pos >= 0; pos--) {
        ledOperate(port, pos);
    }

    *port = 0;
}

void ledOperate(volatile uint8_t *port, int8_t pos) {
    *port = 0;
    *port = 1 << pos;
    _delay_ms(TIMEOUT);
}