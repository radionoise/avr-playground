#ifndef AVR_PLAYGROUND_LED_H
#define AVR_PLAYGROUND_LED_H

#include <avr/io.h>

void ledRunForwardBackward();
void ledRunForward(volatile uint8_t *port);
void ledRunBackward(volatile uint8_t *port);
void ledOperate(volatile uint8_t *port, int8_t pos);

#endif //AVR_PLAYGROUND_LED_H
