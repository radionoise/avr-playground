#include <avr/io.h>
#include <hd44780.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <util/delay.h>
#include "ds18b20.h"
#include "dht22.h"

void main() {
    Hd44780Port portRs = {&DDRA, &PORTA, &PINA, PIN0};
    Hd44780Port portRw = {&DDRA, &PORTA, &PINA, PIN1};
    Hd44780Port portE = {&DDRA, &PORTA, &PINA, PIN2};
    Hd44780Port portD4 = {&DDRB, &PORTB, &PINB, PIN4};
    Hd44780Port portD5 = {&DDRB, &PORTB, &PINB, PIN5};
    Hd44780Port portD6 = {&DDRB, &PORTB, &PINB, PIN6};
    Hd44780Port portD7 = {&DDRB, &PORTB, &PINB, PIN7};

    Hd44780Ports hd44780Ports = {
            .portRs = &portRs,
            .portRw = &portRw,
            .portE = &portE,
            .portD4 = &portD4,
            .portD5 = &portD5,
            .portD6 = &portD6,
            .portD7 = &portD7,
    };
    Hd44780Ports *lcdPortsPtr = &hd44780Ports;

    hd44780Init(lcdPortsPtr);
    hd44780FunctionSet(HD44780_SMALL, HD44780_TWO, lcdPortsPtr);
    hd44780EntryModeSet(false, HD44780_INCREMENT, lcdPortsPtr);
    hd44780DisplayOnOffControl(false, false, true, &hd44780Ports);

    uint8_t degreeSign = 0b11011111;

    char *str1 = malloc(sizeof(char) * 17);
    char *str2 = malloc(sizeof(char) * 17);

    Ds18b20Port tempPort = {&DDRC, &PORTC, &PINC, PIN0};
    Dht22Port dht22Port = {&DDRC, &PORTC, &PINC, PIN1};

    hd44780ClearScreen(lcdPortsPtr);
    hd44780SendString("Loading...", lcdPortsPtr);
    _delay_ms(3000);

    while (true) {
        Dht22Data *data = dht22ReadData(&dht22Port);

        if (data->relativeHumidity == DHT22_NOT_FOUND_ERROR) {
            sprintf(str1, "DHT22 missing   ");
        } else if (data->relativeHumidity == DHT22_CRC_ERROR) {
            sprintf(str1, "DHT22 CRC err   ");
        } else {
            sprintf(str1, "T %.1fC, H %d%%", data->temperature, data->relativeHumidity);
        }

        free(data);

        bool sensorIsPresent = ds18b20IsPresent(&tempPort);

        if (sensorIsPresent) {
            float temp = ds18b20ReadTemperatureSkipRom(&tempPort);

            if (temp != DS18B20_TEMPERATURE_CRC_ERROR) {
                sprintf(str2, "T %.1fC", temp);
            } else {
                sprintf(str2, "DS18B20 CRC err ");
            }

        } else {
            sprintf(str2, "DS18B20 missing ");
        }

        hd44780ReturnHome(lcdPortsPtr);
        hd44780SendString(str1, lcdPortsPtr);
        hd44780SetDdramAddress(0x40, lcdPortsPtr);
        hd44780SendString(str2, lcdPortsPtr);
        hd44780SetDdramAddress(0x40 + 15, lcdPortsPtr);
        hd44780SendData('.', lcdPortsPtr);
        _delay_ms(500);
        hd44780SetDdramAddress(0x40 + 15, lcdPortsPtr);
        hd44780SendData(' ', lcdPortsPtr);

        _delay_ms(2000);
    }
}