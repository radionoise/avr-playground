#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include "hd44780.h"
#include "ds18b20.h"

void main() {
    Hd44780Port portRs = {&DDRA, &PORTA, &PINA, PIN0};
    Hd44780Port portRw = {&DDRA, &PORTA, &PINA, PIN1};
    Hd44780Port portE = {&DDRA, &PORTA, &PINA, PIN2};
    Hd44780Port portD4 = {&DDRB, &PORTB, &PINB, PIN4};
    Hd44780Port portD5 = {&DDRB, &PORTB, &PINB, PIN5};
    Hd44780Port portD6 = {&DDRB, &PORTB, &PINB, PIN6};
    Hd44780Port portD7 = {&DDRB, &PORTB, &PINB, PIN7};

    Hd44780Ports hd44780Ports = {
            .portRs = &portRs,
            .portRw = &portRw,
            .portE = &portE,
            .portD4 = &portD4,
            .portD5 = &portD5,
            .portD6 = &portD6,
            .portD7 = &portD7,
    };

    Hd44780Ports *lcdPorts = &hd44780Ports;
    hd44780Init(lcdPorts);
    hd44780FunctionSet(HD44780_SMALL, HD44780_TWO, lcdPorts);
    hd44780EntryModeSet(false, HD44780_INCREMENT, lcdPorts);

    char *str = malloc(sizeof(char) * 16);

    Ds18b20Port tempPort = {&DDRC, &PORTC, &PINC, PIN0};
    bool sensorIsPresent = ds18b20IsPresent(&tempPort);

    if (sensorIsPresent) {
        uint64_t rom = ds18b20ReadRom(&tempPort);
        if (rom != DS18B20_ROM_CRC_ERROR) {
            uint64_t rom = ds18b20ReadRom(&tempPort);

            while (true) {
                float temperature = ds18b20ReadTemperatureMatchRom(rom, &tempPort);
                sprintf(str, "Temp: %.1fC", temperature);
                hd44780ClearScreen(lcdPorts);
                hd44780SendString(str, lcdPorts);

                _delay_ms(2000);
            }
        } else {
            hd44780ClearScreen(lcdPorts);
            hd44780SendString("ROM CRC Error", lcdPorts);
        }
    } else {
        hd44780ClearScreen(lcdPorts);
        hd44780SendString("Sensor not found", lcdPorts);
    }

    while (true);
}