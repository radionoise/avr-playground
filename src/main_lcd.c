#include <hd44780.h>
#include <util/delay.h>

int main() {
    Hd44780Port portRs = {&DDRA, &PORTA, &PINA, PIN0};
    Hd44780Port portRw = {&DDRA, &PORTA, &PINA, PIN1};
    Hd44780Port portE = {&DDRA, &PORTA, &PINA, PIN2};
    Hd44780Port portD4 = {&DDRB, &PORTB, &PINB, PIN4};
    Hd44780Port portD5 = {&DDRB, &PORTB, &PINB, PIN5};
    Hd44780Port portD6 = {&DDRB, &PORTB, &PINB, PIN6};
    Hd44780Port portD7 = {&DDRB, &PORTB, &PINB, PIN7};

    Hd44780Ports hd44780Ports = {
            .portRs = &portRs,
            .portRw = &portRw,
            .portE = &portE,
            .portD4 = &portD4,
            .portD5 = &portD5,
            .portD6 = &portD6,
            .portD7 = &portD7,
    };

    hd44780Init(&hd44780Ports);
    hd44780FunctionSet(HD44780_SMALL, HD44780_TWO, &hd44780Ports);
    hd44780EntryModeSet(false, HD44780_INCREMENT, &hd44780Ports);

    char str[15] = "Hello new 2.0!";
    int strLength = sizeof(str) / sizeof(char);

    while (true) {
        hd44780ClearScreen(&hd44780Ports);
        hd44780SetDdramAddress(16, &hd44780Ports);
        hd44780SendString(str, &hd44780Ports);
        hd44780SetDdramAddress(16, &hd44780Ports);
        hd44780DisplayOnOffControl(false, false, true, &hd44780Ports);

        for (int i = 0; i < strLength + 16; i++) {
            hd44780CursorOrDisplayShift(HD44780_LEFT, HD44780_DISPLAY, &hd44780Ports);
            _delay_ms(250);
        }
    }
}