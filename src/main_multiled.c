#include <avr/io.h>
#include <util/delay.h>
#include "multiled.h"

int main(void) {
    uint8_t allOut = 0b11111111;
    DDRA = allOut;
    DDRB = allOut;

    while (1) {
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 8; column++) {
                ledOn(row, column, &PORTA, &PORTB);
                _delay_ms(100);
            }
        }
    }
}