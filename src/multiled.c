#include "multiled.h"

void ledOn(int row, int column, volatile uint8_t * powerPort, volatile uint8_t * groundPort) {
    *powerPort = 0;
    *powerPort |= (1 << row);

    *groundPort = 0xff;
    *groundPort &= ~(1 << column);
}