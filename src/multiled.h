#include <avr/io.h>

#ifndef AVR_PLAYGROUND_MULTILED_H
#define AVR_PLAYGROUND_MULTILED_H

void ledOn(int row, int column, volatile uint8_t * powerPort, volatile uint8_t * groundPort);

#endif //AVR_PLAYGROUND_MULTILED_H
